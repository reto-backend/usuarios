package com.pragma.powerup.infrastructure.input.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class UserRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void saveUserSuccessfully() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"string@pragma.com.co\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void saveUserBlankFieldsReturnsBadRequest() throws Exception {
        // Validating all mandatory fields are not blank
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"string@pragma.com.co\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"string@pragma.com.co\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"string@pragma.com.co\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"string@pragma.com.co\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"\",\n" +
                                "  \"email\": \"string@pragma.com.co\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"string@pragma.com.co\",\n" +
                                "  \"password\": \"\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveUserInvalidEmailReturnsBadRequest() throws Exception {
        // Validating all mandatory fields are not blank
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"johnexample.com\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"john@\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"john@@example.com\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"@example.com\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveUserValidCellphoneSuccessfully() throws Exception {
        // Validating all mandatory fields are not blank
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"1234567890\",\n" +
                                "  \"cellphone\": \"+573123456789\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"john@example.com\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void saveUserInvalidCellphoneReturnsBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"John\",\n" +
                                "  \"lastname\": \"Doe\",\n" +
                                "  \"documentNumber\": \"1234567890\",\n" +
                                "  \"cellphone\": \"11111111111111\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"john@example.com\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"John\",\n" +
                                "  \"lastname\": \"Doe\",\n" +
                                "  \"documentNumber\": \"1234567890\",\n" +
                                "  \"cellphone\": \"++1112\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"john@example.com\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveUserInvalidDocumentNumberReturnsBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/propietario")
                        .content("{\n" +
                                "  \"name\": \"John\",\n" +
                                "  \"lastname\": \"Doe\",\n" +
                                "  \"documentNumber\": \"1234567890a\",\n" +
                                "  \"cellphone\": \"+573123456789\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"john@example.com\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveEmpleadoSuccessfully() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/empleado/1")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"string@pragma.com.co\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjE5MjU5OCwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.-07U6K-qP1aHj-PO-SqHwBrIZOgAvf9JRZvCyeKAFvwuSXkKjL93wZxPr08opU7c")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void saveEmpleadoInvalidUserRoleReturnsForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/empleado/1")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"string@pragma.com.co\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ2YWxzNG9vQGdtYWlsLmNvbSIsImV4cCI6MTcwNjE5MDg2Miwicm9sZSI6IlJPTEVfQURNSU5JU1RSQURPUiIsIm5hbWUiOiJFbWlsaW8iLCJpZCI6MjF9.4WMQHIf7w30DWO3MLmIRh-1GvbSWzHmdZBMRFLTlc-Fh4DcwEborlnoPkQ1pFv7C")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    void saveEmpleadoInvalidPropietarioReturnsForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/empleado/1")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"lastname\": \"string\",\n" +
                                "  \"documentNumber\": \"23\",\n" +
                                "  \"cellphone\": \"32\",\n" +
                                "  \"birthDate\": \"2001-12-19\",\n" +
                                "  \"email\": \"string@pragma.com.co\",\n" +
                                "  \"password\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpb0BwcmFnbWEuY29tLmNvIiwiZXhwIjoxNzA2MTkzMzE1LCJyb2xlIjoiUk9MRV9QUk9QSUVUQVJJTyIsIm5hbWUiOiJwbGF6b2xldGEiLCJpZCI6MjZ9.U0rxQYjk0iSwLo6GDDQsJDJezY4dfGFUTuXjO9F5C0HLAqZSTsosMQhlXpYT3APM")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }
}