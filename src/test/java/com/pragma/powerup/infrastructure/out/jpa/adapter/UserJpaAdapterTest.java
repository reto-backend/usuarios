package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.domain.model.UserModel;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import com.pragma.powerup.infrastructure.feign.client.IPlazoletaFeignClient;
import com.pragma.powerup.infrastructure.out.jpa.entity.UserEntity;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IEmpleadoEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IUserEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IEmpleadoRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class UserJpaAdapterTest {

    @Mock
    private IUserRepository userRepository;
    @Mock
    private IUserEntityMapper userEntityMapper;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private IEmpleadoRepository empleadoRepository;
    @Mock
    private IEmpleadoEntityMapper empleadoEntityMapper;
    @Mock
    private IPlazoletaFeignClient plazoletaFeignClient;
    @InjectMocks
    private UserJpaAdapter userJpaAdapter;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        userJpaAdapter = new UserJpaAdapter(
                userRepository,
                userEntityMapper,
                empleadoRepository,
                empleadoEntityMapper,
                passwordEncoder,
                plazoletaFeignClient
        );
    }

    @Test
    void saveUserValidUserModelReturnsSavedUserModel() {
        UserModel userModel = new UserModel();
        UserEntity mockEntity = new UserEntity();
        when(userEntityMapper.toEntity(userModel)).thenReturn(mockEntity);
        when(userRepository.save(any())).thenReturn(mockEntity);
        when(passwordEncoder.encode(mockEntity.getPassword())).thenReturn("hashedPassword");
        when(userEntityMapper.toUserModel(mockEntity)).thenReturn(userModel);

        UserModel savedUser = userJpaAdapter.saveUser(userModel);

        assertEquals(userModel, savedUser);
    }

    @Test
    void getUserExistingUser() {
        UserEntity userEntity = new UserEntity();
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(userEntity));

        UserModel expectedUserModel = new UserModel();
        when(userEntityMapper.toUserModel(userEntity)).thenReturn(expectedUserModel);

        UserModel actualUserModel = userJpaAdapter.getUser(123L);

        assertEquals(expectedUserModel, actualUserModel);
    }

    @Test
    void getUserUserNotFound() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NoDataFoundException.class, () -> userJpaAdapter.getUser(456L));
    }
}