package com.pragma.powerup.infrastructure.out.jpa.validators;

import com.pragma.powerup.domain.shared.BusinessRules;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AgeValidatorTest {

    @Mock
    private ConstraintValidatorContext cxt;

    @InjectMocks
    private AgeValidator ageValidator;

    private int legalAge;
    private Calendar current;
    private Calendar dobValid;
    private Calendar dobInvalid;

    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);

        legalAge = BusinessRules.LEGAL_AGE;

        current = Calendar.getInstance();
        dobValid = Calendar.getInstance();
        dobInvalid = Calendar.getInstance();

        dobValid.set(Calendar.YEAR, current.get(Calendar.YEAR) - legalAge);
        dobInvalid.set(Calendar.YEAR, current.get(Calendar.YEAR) - (legalAge - 1));
    }

    @Test
    void isValid() {
        assertTrue(ageValidator.isValid(dobValid.getTime(), cxt));
        assertFalse(ageValidator.isValid(dobInvalid.getTime(), cxt));
    }
}