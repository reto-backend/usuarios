package com.pragma.powerup.domain.shared;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoleEnumTest {

    @Test
    void getValueReturnsCorrectValues() {
        assertEquals(1L, RoleEnum.ADMINISTRADOR.getValue());
        assertEquals(2L, RoleEnum.PROPIETARIO.getValue());
        assertEquals(3L, RoleEnum.EMPLEADO.getValue());
        assertEquals(4L, RoleEnum.CLIENTE.getValue());
    }
}