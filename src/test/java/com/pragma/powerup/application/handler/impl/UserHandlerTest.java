package com.pragma.powerup.application.handler.impl;

import com.pragma.powerup.application.dto.request.UserRequestDto;
import com.pragma.powerup.application.dto.response.UserResponseDto;
import com.pragma.powerup.application.mapper.IUserRequestMapper;
import com.pragma.powerup.application.mapper.IUserResponseMapper;
import com.pragma.powerup.domain.api.IUserServicePort;
import com.pragma.powerup.domain.model.UserModel;
import com.pragma.powerup.domain.shared.RoleEnum;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UserHandlerTest {

    @Mock
    private IUserServicePort userServicePort;

    @Mock
    private IUserRequestMapper userRequestMapper;

    @Mock
    private IUserResponseMapper userResponseMapper;

    @InjectMocks
    private UserHandler userHandler;

    public UserHandlerTest() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveUserSuccessfully() {
        UserRequestDto userRequestDto = new UserRequestDto();
        UserModel userModel = new UserModel();
        when(userRequestMapper.toUser(userRequestDto)).thenReturn(userModel);

        Long roleId = RoleEnum.PROPIETARIO.getValue();
        userHandler.saveUser(userRequestDto, roleId);

        verify(userRequestMapper).toUser(userRequestDto);
        verify(userServicePort).saveUser(userModel);
    }

    @Test
    void getUserSuccess(){
        UserModel expectedUser = new UserModel();
        when(userServicePort.getUser(anyLong())).thenReturn(expectedUser);

        UserResponseDto expectedResponse = new UserResponseDto();
        when(userResponseMapper.toResponse(expectedUser)).thenReturn(expectedResponse);

        UserResponseDto actualResponse = userHandler.getUser(123L); // Pass any ID for testing

        assertEquals(expectedResponse, actualResponse);
    }
}