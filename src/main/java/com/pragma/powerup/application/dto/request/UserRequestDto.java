package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserRequestDto {
    private String name;
    private String lastname;
    private String documentNumber;
    private String cellphone;
    private Date birthDate;
    private String email;
    private String password;
}
