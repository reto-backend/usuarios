package com.pragma.powerup.application.handler.impl;

import com.pragma.powerup.application.dto.request.UserRequestDto;
import com.pragma.powerup.application.dto.response.EmpleadoResponseDto;
import com.pragma.powerup.application.dto.response.UserResponseDto;
import com.pragma.powerup.application.handler.IUserHandler;
import com.pragma.powerup.application.mapper.IEmpleadoResponseMapper;
import com.pragma.powerup.application.mapper.IUserRequestMapper;
import com.pragma.powerup.application.mapper.IUserResponseMapper;
import com.pragma.powerup.domain.api.IUserServicePort;
import com.pragma.powerup.domain.model.EmpleadoModel;
import com.pragma.powerup.domain.model.RoleModel;
import com.pragma.powerup.domain.model.UserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class UserHandler implements IUserHandler {

    private final IUserServicePort userServicePort;
    private final IUserRequestMapper userRequestMapper;
    private final IUserResponseMapper userResponseMapper;

    private final IEmpleadoResponseMapper empleadoResponseMapper;

    @Override
    public void saveUser(UserRequestDto userRequestDto, Long roleId) {
        UserModel userModel = userRequestMapper.toUser(userRequestDto);
        RoleModel roleModel = new RoleModel(roleId);
        userModel.setRole(roleModel);
        userServicePort.saveUser(userModel);
    }

    @Override
    public void saveEmpleado(UserRequestDto userRequestDto, Long roleId, Long restaurantId, String token) {
        EmpleadoModel empleadoModel = userRequestMapper.toEmpleado(userRequestDto);
        empleadoModel.setRestaurantId(restaurantId);

        RoleModel roleModel = new RoleModel(roleId);
        empleadoModel.setRole(roleModel);
        userServicePort.saveEmpleado(empleadoModel, token);
    }

    @Override
    public UserResponseDto getUser(Long id) {
        return userResponseMapper.toResponse(userServicePort.getUser(id));
    }

    @Override
    public EmpleadoResponseDto getEmpleado(Long id) {
        return empleadoResponseMapper.toResponse(userServicePort.getEmpleado(id));
    }
}
