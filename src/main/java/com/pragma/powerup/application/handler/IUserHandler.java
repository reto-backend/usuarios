package com.pragma.powerup.application.handler;

import com.pragma.powerup.application.dto.request.UserRequestDto;
import com.pragma.powerup.application.dto.response.EmpleadoResponseDto;
import com.pragma.powerup.application.dto.response.UserResponseDto;

public interface IUserHandler {
    void saveUser(UserRequestDto userRequestDto, Long roleId);

    void saveEmpleado(UserRequestDto userRequestDto, Long roleId, Long restaurantId, String token);

    UserResponseDto getUser(Long id);

    EmpleadoResponseDto getEmpleado(Long id);
}
