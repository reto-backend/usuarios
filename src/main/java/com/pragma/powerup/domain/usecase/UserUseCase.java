package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.api.IUserServicePort;
import com.pragma.powerup.domain.model.EmpleadoModel;
import com.pragma.powerup.domain.model.UserModel;
import com.pragma.powerup.domain.spi.IUserPersistencePort;

public class UserUseCase implements IUserServicePort {
    private final IUserPersistencePort userPersistencePort;

    public UserUseCase(IUserPersistencePort userPersistencePort) {

        this.userPersistencePort = userPersistencePort;
    }

    @Override
    public void saveUser(UserModel userModel) {
        userPersistencePort.saveUser(userModel);
    }

    @Override
    public void saveEmpleado(EmpleadoModel empleadoModel, String token) {
        userPersistencePort.saveEmpleado(empleadoModel, token);
    }
    @Override
    public UserModel getUser(Long id) {
        return userPersistencePort.getUser(id);
    }

    @Override
    public EmpleadoModel getEmpleado(Long id) {
        return userPersistencePort.getEmpleado(id);
    }
}
