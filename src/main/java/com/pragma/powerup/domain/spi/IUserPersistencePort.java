package com.pragma.powerup.domain.spi;

import com.pragma.powerup.domain.model.EmpleadoModel;
import com.pragma.powerup.domain.model.UserModel;

public interface IUserPersistencePort {

    UserModel saveUser(UserModel userModel);

    EmpleadoModel saveEmpleado(EmpleadoModel empleadoModel, String token);
    UserModel getUser(Long id);

    EmpleadoModel getEmpleado(Long id);
}
