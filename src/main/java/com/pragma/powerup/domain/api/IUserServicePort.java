package com.pragma.powerup.domain.api;

import com.pragma.powerup.domain.model.EmpleadoModel;
import com.pragma.powerup.domain.model.UserModel;

public interface IUserServicePort {

    void saveUser(UserModel userModel);

    void saveEmpleado(EmpleadoModel empleadoModel, String token);

    UserModel getUser(Long id);

    EmpleadoModel getEmpleado(Long id);
}
