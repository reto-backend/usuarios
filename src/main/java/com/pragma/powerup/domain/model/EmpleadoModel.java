package com.pragma.powerup.domain.model;

import java.util.Date;

public class EmpleadoModel extends UserModel{
    private Long restaurantId;

    public EmpleadoModel() {
    }

    public EmpleadoModel(Long id, String name, String lastname, String documentNumber, String cellphone, Date birthDate, String email, String password, RoleModel role) {
        super(id, name, lastname, documentNumber, cellphone, birthDate, email, password, role);
    }

    public EmpleadoModel(Long id, String name, String lastname, String documentNumber, String cellphone, Date birthDate, String email, String password, RoleModel role, Long restaurantId) {
        super(id, name, lastname, documentNumber, cellphone, birthDate, email, password, role);
        this.restaurantId = restaurantId;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }
}
