package com.pragma.powerup.domain.shared;

public enum RoleEnum {
    ADMINISTRADOR(1L),
    PROPIETARIO(2L),
    EMPLEADO(3L),
    CLIENTE(4L);

    private final Long value;

    RoleEnum(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }
}

