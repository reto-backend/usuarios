package com.pragma.powerup.domain.shared;

public final class BusinessRules {
    private BusinessRules() {
    }
    public static final int LEGAL_AGE = 18;
}
