package com.pragma.powerup.infrastructure.input.rest;

import com.pragma.powerup.application.dto.request.UserRequestDto;
import com.pragma.powerup.application.dto.response.EmpleadoResponseDto;
import com.pragma.powerup.application.dto.response.UserResponseDto;
import com.pragma.powerup.application.handler.IUserHandler;
import com.pragma.powerup.domain.shared.RoleEnum;
import com.pragma.powerup.infrastructure.out.jpa.repository.IEmpleadoRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserRestController {
    private final IUserHandler userHandler;

    @Operation(summary = "Create Propietario")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Propietario created", content = @Content),
            @ApiResponse(responseCode = "400", description = "Wrong Propietario creation data", content = @Content)
    })
    @PostMapping("/propietario")
    public ResponseEntity<Void> savePropietario(@RequestBody UserRequestDto userRequestDto) {
        userHandler.saveUser(userRequestDto, RoleEnum.PROPIETARIO.getValue());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Create Empleado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Empleado created", content = @Content),
            @ApiResponse(responseCode = "400", description = "Wrong Empleado creation data", content = @Content),
            @ApiResponse(responseCode = "404", description = "Restaurant not found", content = @Content)
    })
    @PostMapping("/empleado/{restaurantId}")
    public ResponseEntity<Void> saveEmpleado(@RequestBody UserRequestDto userRequestDto,
                                         @PathVariable Long restaurantId,
                                         @RequestHeader("Authorization") String token) {
        userHandler.saveEmpleado(userRequestDto, RoleEnum.EMPLEADO.getValue(), restaurantId, token);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Create Cliente")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Cliente created", content = @Content),
            @ApiResponse(responseCode = "400", description = "Wrong Cliente creation data", content = @Content)
    })
    @PostMapping("/cliente")
    public ResponseEntity<Void> saveCliente(@RequestBody UserRequestDto userRequestDto) {
        userHandler.saveUser(userRequestDto, RoleEnum.CLIENTE.getValue());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Read User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User read", content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    @GetMapping("/user/{id}")
    public ResponseEntity<UserResponseDto> getUser(@PathVariable Long id) {
        return ResponseEntity.ok(userHandler.getUser(id));
    }

    @Operation(summary = "Read Empleado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Empleado read", content = @Content),
            @ApiResponse(responseCode = "404", description = "Empleado not found", content = @Content)
    })
    @GetMapping("/empleado/{id}")
    public ResponseEntity<EmpleadoResponseDto> getEmpleado(@PathVariable Long id) {
        return ResponseEntity.ok(userHandler.getEmpleado(id));
    }
}
