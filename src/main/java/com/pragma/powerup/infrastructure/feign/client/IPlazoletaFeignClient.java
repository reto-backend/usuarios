package com.pragma.powerup.infrastructure.feign.client;

import com.pragma.powerup.application.dto.response.RestaurantResponseDto;
import com.pragma.powerup.infrastructure.feign.configuration.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "plazoleta-service", url = "${plazoleta.ms.api.base-url}", configuration = FeignClientConfig.class)
public interface IPlazoletaFeignClient {

    @GetMapping(value = "/restaurant/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    RestaurantResponseDto getRestaurant(@PathVariable("id") Long id);
}
