package com.pragma.powerup.infrastructure.out.jpa.validators;

import com.pragma.powerup.domain.shared.BusinessRules;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Calendar;
import java.util.Date;

public class AgeValidator implements ConstraintValidator<MinimumAge, Date> {

    @Override
    public void initialize(MinimumAge date) {
        // Nothing to do here
    }

    @Override
    public boolean isValid(Date date, ConstraintValidatorContext cxt) {
        // Check user is of legal age
        if (date == null) {
            return false;
        }
        return calculateAgeInYears(date) >= BusinessRules.LEGAL_AGE;
    }

    private int calculateAgeInYears(Date dateOfBirth) {

        Calendar dob = Calendar.getInstance();
        dob.setTime(dateOfBirth);

        Calendar current = Calendar.getInstance();

        int age = current.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        // Adjust age if the current date is before the birthdate for this year
        if (current.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        return age;
    }
}
