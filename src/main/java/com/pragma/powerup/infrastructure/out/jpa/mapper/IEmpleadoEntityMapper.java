package com.pragma.powerup.infrastructure.out.jpa.mapper;

import com.pragma.powerup.domain.model.EmpleadoModel;
import com.pragma.powerup.infrastructure.out.jpa.entity.EmpleadoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IEmpleadoEntityMapper {

    EmpleadoEntity toEntity(EmpleadoModel empleado);

    EmpleadoModel toEmpleadoModel(EmpleadoEntity empleadoEntity);
}
