package com.pragma.powerup.infrastructure.out.jpa.entity;

import com.pragma.powerup.domain.shared.BusinessRules;
import com.pragma.powerup.infrastructure.out.jpa.validators.MinimumAge;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Inheritance(strategy = InheritanceType.JOINED)
public class UserEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    @NotBlank(message = "Name is required")
    private String name;
    @NotBlank(message = "Lastname is required")
    private String lastname;
    @NotBlank(message = "Document number is required")
    @Pattern(regexp = "^(\\d+)$", message = "Document number is not numeric")
    private String documentNumber;
    @NotBlank(message = "Cellphone is required")
    @Pattern(regexp = "^\\+?\\d{1,12}$", message = "Cellphone is not valid")
    private String cellphone;
    @NotNull(message = "Birth date is required")
    @Temporal(TemporalType.DATE)
    @MinimumAge(message = "Propietario must be " + BusinessRules.LEGAL_AGE + " years or older")
    private Date birthDate;
    @NotBlank(message = "Email is required")
    @Email(message = "Email is not valid")
    private String email;
    @NotBlank(message = "Password is required")
    private String password;
    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private RoleEntity role;
}
