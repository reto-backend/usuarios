package com.pragma.powerup.infrastructure.out.jpa.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AgeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MinimumAge {
    String message() default "User does not meet the required age";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
