package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.application.dto.response.RestaurantResponseDto;
import com.pragma.powerup.domain.model.EmpleadoModel;
import com.pragma.powerup.domain.model.UserModel;
import com.pragma.powerup.domain.spi.IUserPersistencePort;
import com.pragma.powerup.infrastructure.exception.InvalidPropietarioException;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import com.pragma.powerup.infrastructure.feign.client.IPlazoletaFeignClient;
import com.pragma.powerup.infrastructure.out.jpa.entity.EmpleadoEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.UserEntity;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IEmpleadoEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IUserEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IEmpleadoRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IUserRepository;
import com.pragma.powerup.infrastructure.security.TokenUtils;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

@RequiredArgsConstructor
public class UserJpaAdapter implements IUserPersistencePort {

    private final IUserRepository userRepository;
    private final IUserEntityMapper userEntityMapper;

    private final IEmpleadoRepository empleadoRepository;
    private final IEmpleadoEntityMapper empleadoEntityMapper;

    private final PasswordEncoder passwordEncoder;

    private final IPlazoletaFeignClient plazoletaFeignClient;


    @Override
    public UserModel saveUser(UserModel userModel) {
        UserEntity userEntity = userRepository.save(userEntityMapper.toEntity(userModel));
        String encodedPassword = passwordEncoder.encode(userEntity.getPassword());
        userEntity.setPassword(encodedPassword);
        return userEntityMapper.toUserModel(userEntity);
    }

    @Override
    public EmpleadoModel saveEmpleado(EmpleadoModel empleadoModel, String token) {
        validatePropietario(token, empleadoModel.getRestaurantId());

        EmpleadoEntity empleadoEntity = empleadoRepository.save(empleadoEntityMapper.toEntity(empleadoModel));
        String encodedPassword = passwordEncoder.encode(empleadoEntity.getPassword());
        empleadoEntity.setPassword(encodedPassword);
        return empleadoEntityMapper.toEmpleadoModel(empleadoEntity);
    }

    @Override
    public UserModel getUser(Long id) {
        Optional<UserEntity> userEntity = userRepository.findById(id);
        if (userEntity.isEmpty()) {
            throw new NoDataFoundException("User with id " + id + " not found");
        }
        return userEntityMapper.toUserModel(userEntity.get());
    }

    @Override
    public EmpleadoModel getEmpleado(Long id) {
        Optional<EmpleadoEntity> empleadoEntity = empleadoRepository.findById(id);
        if (empleadoEntity.isEmpty()) {
            throw new NoDataFoundException("Empleado with id " + id + " not found");
        }
        return empleadoEntityMapper.toEmpleadoModel(empleadoEntity.get());
    }

    private void validatePropietario(String token, Long restaurantId) {
        try {
            Long claimedPropietarioId = TokenUtils.getUserId(token);
            if (claimedPropietarioId == null) {
                throw new InvalidPropietarioException("You are not the owner of the restaurant");
            }
            RestaurantResponseDto restaurant = plazoletaFeignClient.getRestaurant(restaurantId);
            Long propietarioId = restaurant.getPropietarioId();
            if (!claimedPropietarioId.equals(propietarioId)) {
                throw new InvalidPropietarioException("You are not the owner of the restaurant");
            }
        } catch (FeignException e) {
            throw new NoDataFoundException("Restaurant with id " + restaurantId + " not found");
        }
    }
}